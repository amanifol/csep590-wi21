import {expect} from "chai";
import {EngExp} from "../src/engexp";

describe("custom", () => {

    it("should detect an extra beginLevel and throw an invalid beginLevel error", () => {

        const e = new EngExp()
                .startOfLine()
                    .beginLevel() // extra beginLevel
                        .beginLevel()
                            .match('a')
                        .endLevel()
                        .match('b')
                .endOfLine();
        
        expect(() => {
            e.asRegExp() // extra beginLevels detected when converting to RegEx
        })
        .to.throw('invalid beginLevel');
    });

    it("should detect an extra endLevel and throw an invalid endLevel error", () => {

        const e = new EngExp()
                .startOfLine()
                    .beginLevel()
                        .match('a')
                    .endLevel();
        
        expect(() => {
            e.endLevel() // appending extra endLevel detected immediately
        })
        .to.throw('invalid endLevel');
    });

    it("should detect an extra beginCapture and throw an invalid beginCapture error", () => {

        const e = new EngExp()
                .startOfLine()
                    .beginCapture() // extra beginCapture
                        .beginCapture()
                            .match('a')
                        .endCapture()
                        .match('b')
                .endOfLine();
        
        expect(() => {
            e.asRegExp() // extra beginCaptures detected when converting to RegEx
        })
        .to.throw('invalid beginCapture');
    });

    it("should detect an extra endCapture and throw an invalid endCapture error", () => {

        const e = new EngExp()
                .startOfLine()
                    .beginCapture()
                        .match('a')
                    .endCapture();
        
        expect(() => {
            e.endCapture() // appending extra endCapture detected immediately
        })
        .to.throw('invalid endCapture');
    });

    it("should detect a mismatched endLevel and throw an invalid endLevel error", () => {

        const e = new EngExp()
                .startOfLine()
                    .beginCapture()
                        .match('a')
                    .endCapture()
                    .beginCapture()
                        .match('b');
        
        expect(() => {
            e.endLevel() // appending mismatched endLevel detected immediately
        })
        .to.throw('invalid endLevel');
    });

    it("should detect a mismatched endCapture and throw an invalid endCapture error", () => {

        const e = new EngExp()
                .startOfLine()
                    .beginLevel()
                        .match('a')
                    .endLevel()
                    .beginLevel()
                        .match('b');
        
        expect(() => {
            e.endCapture() // appending mismatched endCapture detected immediately
        })
        .to.throw('invalid endCapture');
    });
});
